Stress APP
=======  

Vamos ahora a ver un poco de pruebas de rendimiento, para poder empezar a ver la utilidad de usar Swarm, para ello vamos a usar la
imagen: swarmgs/nodewebstress

Iniciamos el contenedor: 

> docker run -dit --name webstress -p 3000:3000 swarmgs/nodewebstress

Validamos ahora: 

> http://localhost:3000/customer/1 

Ahora, si, antes de empezar a realizar nuestras pruebas de stress, debemos instalar lo siguiente:

> yum install httpd-tools -y

Con ab instalado, procedemos a realizar lo siguiente: 

> ab -n 100 http://127.0.0.1:3000/customer/1 

## OBS.

Revisar: "Requests per second"

Pero, esto solo es una sola solicitud, es decir una sola "conexion" a nuestro aplicativo, que pasaria si fuera ahora 4 pero en simultaneo ( concurrentes )

> ab -n 1000 -c 4 http://127.0.0.1:3000/customer/1

Revisemos el valor de "Requests per second", este se eleva.

Supongamos que deseamos tener el aplicativo CustomerAPI "replicado" o desplegado en mas contenedores, alguna idea??

Vamos ahora a deployar otro contenedor customerAPI, este va a escuchar el puerto 3001 (recordemos que nuestro contenedor inicial escucha el puerto 3000)

> docker run -dit --name webstress1 -p 3001:3000 swarmgs/nodewebstress

Lanzamos nuevamente las conexciones en simultaneo:

> ab -n 1000 -c 4 http://127.0.0.1:3001/customer/1 

Funciona, pero no es muy didactico hacerlo asi, cierto? esto debido a que simula lo REAL, para un escenario real, haremos lo sgt:

Antes, ejecutaremos esto:

> yum -y install parallel.noarch

> echo "http://localhost:3000/customer/1\nhttp://localhost:3001/customer/2" | parallel -j 2 "ab -n 1000 {.}"

Revisemos "Requests per second"

Vemos que puede atender una cantidad considerable de peticiones ;)

De esta manera, podemos "saturar" nuestro Nodo Docker, hasta que este aguante a nivel de su infraestructura. 
