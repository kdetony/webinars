Webinar Docker Tips&Tricks
========= 

### Parte 1

Entramos a la carpeta **ejm1** 

Vamos a ejecutar lo sgt: 

> docker build -t imgpy . 

Ahora iniciamos el contenedor: 

> docker run -dit -p 8080:8080 --name hola-python imgpy

Ejecutamos:

> docker exec -it hola-python sh

Podemos hacer tambien un curl al contendor.

Ahora nos ubicamos en la carpeta **ejm2**

> docker build -t imgpy1 . 

> docker run -dit -p 9090:9090 --name hola-python1  imgpy1


### Parte 2

Entramos a la carpeta Stress

Vamos a usar la imagen: 

swarmgs/nodewebstress

Los siguientes aplicativos: 

* ab
* parallel 
* curl 
