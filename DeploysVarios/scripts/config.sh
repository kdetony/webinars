#!/bin/bash

for i in $(cat lista);do ssh root@$i yum update -y && yum -y install epel-release;done
for i in $(cat lista);do ssh root@$i yum -y install vim vim-enhanced wget curl rsync;done
for i in $(cat lista);do  scp get-docker.sh root@$i:/root;done
for i in $(cat lista);do ssh root@$i sh /root/get-docker.sh;done
