## Docker swarm

Previamente, vamos a trabajar con esta infraestructura: 

* 1 Server NFS
* 1 Server Docker Master
* 1 Server Docker Node

En el nodo master:

> docker swarm init

Con el token generado, lo copiamos y registramos el nodo, para ello, nos ubicamos en el nodo docker:

> docker swarm join --token SWMTKN-1-xxxx 192.168.16.90:2377

### OBS.
Si queremos imprimir el token: 

> docker swarm join-token worker/manager


Con nuestro Cluster arriba, empezamos: 


1.-creamos un "service" 

> docker service create --detach=true --replicas 1 --name pinger centos ping localhost

2.-Validamos el servicio creado

> docker service ls

3.-Y ahora vemos en que nodo esta ejecutandose

> docker service ps pinger

4.-Entramos al nodo donde esta el contenedor y hacemos:

> docker logs -f ID_CONTAINER

5.-Escalamiento

> docker service scale pinger=3

6.- Volvemos a ver en que nodos esta desplegado el contenedor:

> docker service ps pinger

### OBS.
Para eliminar un SERVICE, usamos:

> docker service rm NAME / ID_SERVICE

7.- Vamos a deployar un balanceador apache de ejemplo:

> docker service create --detach=true --replicas 3 --publish 80:80 --name httpd sweh/test

y ejecutamos en los nodos:

> curl localhost/cgi-bin/t.cgi

## STACK

simplemente es crear todo lo que se conoce via docker-compose

```
stack.yml

version: "3"

networks:
  webapp:
  appdb:

volumes:
  db-data:

services:
  web:
    image: sweh/test
    networks:
      - webapp
    ports:
      - 80:80

  app:
    image: centos
    networks:
      - webapp
      - appdb
    entrypoint: /bin/sh
    stdin_open: true
    tty: true

  db:
    image: mysql:5.5
    networks:
      - appdb
    environment:
      - MYSQL_ROOT_PASSWORD=foobar
      - MYSQL_DATABASE=mydb1
    volumes:
      - db-data:/var/lib/mysql
```
   		
Tener en consideracion que volume **db-data** debe ser compartido via NFS como recursos para todos los nodos.
caso contrario no habrá persistencia.

8.- Lo ejecutamos:

> docker stack deploy -c stack.yaml appdemo

### OBS.

si colocamos appdemo vemos lo que pasa :)

9.- Para ver los recursos creados por el STACK o deploy:

> docker stack ls

10.- Otra forma de validar los servicios creados:

> docker stack services appdemo


## VOLUME PERSISTENTE 

En este caso vamos a ejecutar lo sgt:

```
docker service create --mount type=volume,volume-opt=o=addr=192.168.16.101,volume-opt=device=:/data,volume-opt=type=nfs,source=SGT2,target=/usr/share/nginx/html --replicas 4 --name webnginx nginx
```

Entramos a un contenedor y Validamos !!!! 

Si queremos publicar un puerto de acceso para ver acceder al contenedor deployado:

> docker service update --publish-add 9090:80 webnginx

