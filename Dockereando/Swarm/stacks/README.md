## Creando RED

1. Creamos una red overlay en el nodo master: 

> docker network create --driver=overlay --attachable core-infra

2. Deployamos el stack:

> docker stack deploy -c app.yml

## Escalando contendores:

> docker scale service NAME_SERVICE=CANTIDAD
