## Servidor Independiente NFS 

1. Instalamos NFS:

> yum -y install nfs-utils 

2. Editamos /etc/exports, previamente:


> mkdir -p /data 

```
chmod -R 755 /var/nfsshare

chown nfsnobody:nfsnobody /var/nfsshare
```

> /data  192.168.16.0/24(rw,sync,no_root_squash,no_all_squash)

3. Iniciamos el servicio:

> systemctl enable nfs-server && systemctl start nfs-server


## En cada uno de los Servers Docker

1. Instalamos nfs-utils

> yum -y install nfs-utils

2. Creamos la carpeta:

> mkdir -p /mnt/swarm 

3. Montamos :

> mount -t nfs 192.168.16.101:/data /mnt/swarm/
