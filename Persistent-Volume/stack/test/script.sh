#!/usr/bin/env bash

search=$1
shift 1
command="$@"

CONTAINER=$(docker ps -q --filter="name=_$search")
CONTAINER_NAME=$(docker ps --filter="name=_$search" | rev | awk '{print $1}' | sed -n '1!p' | rev)

printf "[executing on container $CONTAINER_NAME]\n\n"
docker exec -it $CONTAINER bash -lc "$command"
